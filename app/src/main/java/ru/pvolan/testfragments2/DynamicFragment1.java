package ru.pvolan.testfragments2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DynamicFragment1 extends Fragment {



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "DynamicFragment1 onCreate()");
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return null;

    }

    @Override
    public void onStart() {
        Log.i("TEST", "DynamicFragment1 onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "DynamicFragment1 onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "DynamicFragment1 onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "DynamicFragment1 onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "DynamicFragment1 onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "DynamicFragment1 onDestroy()");
        super.onDestroy();
    }


}
