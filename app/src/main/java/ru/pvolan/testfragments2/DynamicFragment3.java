package ru.pvolan.testfragments2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DynamicFragment3 extends Fragment {



    private int param;


    public static DynamicFragment3 getInstance(int param) {
        DynamicFragment3 f = new DynamicFragment3();
        Bundle b = new Bundle();
        b.putInt("Param", param);
        f.setArguments(b);
        return f;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "DynamicFragment3 onCreate()");
        super.onCreate(savedInstanceState);
        param = getArguments().getInt("Param");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i("TEST", "DynamicFragment3 onCreateView()");

        TextView tv = new TextView(getActivity());
        tv.setBackgroundColor(0xff9999ff);
        tv.setText("DynamicFragment3 - param " + param);

        return tv;

    }

    @Override
    public void onStart() {
        Log.i("TEST", "DynamicFragment3 onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "DynamicFragment3 onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "DynamicFragment3 onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "DynamicFragment3 onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "DynamicFragment3 onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "DynamicFragment3 onDestroy()");
        super.onDestroy();
    }


}
