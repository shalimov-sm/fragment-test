package ru.pvolan.testfragments2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class StaticFragment extends Fragment {



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "StaticFragment onCreate()");
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i("TEST", "StaticFragment onCreateView()");

        TextView tv = new TextView(getActivity());
        tv.setBackgroundColor(0xffff00ff);
        tv.setText("Static fragment");

        return tv;

    }

    @Override
    public void onStart() {
        Log.i("TEST", "StaticFragment onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "StaticFragment onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "StaticFragment onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "StaticFragment onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "StaticFragment onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "StaticFragment onDestroy()");
        super.onDestroy();
    }


}
