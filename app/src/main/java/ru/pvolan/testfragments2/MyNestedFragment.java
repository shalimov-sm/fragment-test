package ru.pvolan.testfragments2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MyNestedFragment extends Fragment {


    public static MyNestedFragment newInstance() {
        
        Bundle args = new Bundle();
        
        MyNestedFragment fragment = new MyNestedFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "MyNestedFragment onCreate()");
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i("TEST", "MyNestedFragment onCreateView()");

        TextView tv = new TextView(getActivity());
        tv.setBackgroundColor(0xff00ff00);
        tv.setText("MyNestedFragment");

        return tv;

    }

    @Override
    public void onStart() {
        Log.i("TEST", "MyNestedFragment onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "MyNestedFragment onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "MyNestedFragment onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "MyNestedFragment onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "MyNestedFragment onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "MyNestedFragment onDestroy()");
        super.onDestroy();
    }


}
