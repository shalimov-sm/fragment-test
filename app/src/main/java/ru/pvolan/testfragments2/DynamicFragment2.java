package ru.pvolan.testfragments2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class DynamicFragment2 extends Fragment {


    private int i = 0;


    static DynamicFragment2 newInstance(Class activityClass) {

        Bundle args = new Bundle();

        DynamicFragment2 fragment = new DynamicFragment2();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "DynamicFragment2 onCreate()");
        super.onCreate(savedInstanceState);




        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        MyNestedFragment f = MyNestedFragment.newInstance();
        ft.addToBackStack(null);
        ft.replace(R.id.nested_fragment_container, f);
        ft.commit();

    }











    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dyn_fragment_2, null);
        return v;
    }

    @Override
    public void onStart() {
        Log.i("TEST", "DynamicFragment2 onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "DynamicFragment2 onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "DynamicFragment2 onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "DynamicFragment2 onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {

        Log.i("TEST", "DynamicFragment2 onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "DynamicFragment2 onDestroy()");
        super.onDestroy();
    }


}
