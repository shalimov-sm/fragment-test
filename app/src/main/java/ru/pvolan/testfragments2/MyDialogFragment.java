package ru.pvolan.testfragments2;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MyDialogFragment extends DialogFragment {



    private int param;


    public static MyDialogFragment getInstance(int param) {
        MyDialogFragment f = new MyDialogFragment();
        Bundle b = new Bundle();
        b.putInt("Param", param);
        f.setArguments(b);
        return f;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "MyDialogFragment onCreate()");
        super.onCreate(savedInstanceState);
        param = getArguments().getInt("Param");
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());

        //View v = null/*inflate*/;
        //final EditText editText = v.findViewById(R.id.some_edit_text);
        //b.setView(v);

        b.setTitle("Test");
        b.setNeutralButton("Wow!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "really wow!", Toast.LENGTH_LONG).show();

                //String s = editText.getText().toString();
                //((ActivityCallback)getActivity()).onDialogOkClicked(s);
            }
        });
        b.setMessage("Very important message");

        return b.create();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i("TEST", "MyDialogFragment onCreateView()");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.i("TEST", "MyDialogFragment onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "MyDialogFragment onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "MyDialogFragment onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "MyDialogFragment onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "MyDialogFragment onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "MyDialogFragment onDestroy()");
        super.onDestroy();
    }


}
