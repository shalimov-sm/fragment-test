package ru.pvolan.testfragments2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class StackFragment2 extends Fragment {



    private int param;


    public static StackFragment2 getInstance(int param) {
        StackFragment2 f = new StackFragment2();
        Bundle b = new Bundle();
        b.putInt("Param", param);
        f.setArguments(b);
        return f;
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "StackFragment2 onCreate()");
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        param = getArguments().getInt("Param");
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Log.i("TEST", "StackFragment2 onCreateView()");

        TextView tv = new TextView(getActivity());
        tv.setBackgroundColor(0xffff9955);
        tv.setText("StackFragment2 - param " + param);

        return tv;

    }

    @Override
    public void onStart() {
        Log.i("TEST", "StackFragment2 onStart()");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i("TEST", "StackFragment2 onResume()");
        super.onResume();
    }


    @Override
    public void onPause() {
        Log.i("TEST", "StackFragment2 onPause()");
        super.onPause();
    }


    @Override
    public void onStop() {
        Log.i("TEST", "StackFragment2 onStop()");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i("TEST", "StackFragment2 onDestroyView()");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i("TEST", "StackFragment2 onDestroy()");
        super.onDestroy();
    }


}
