package ru.pvolan.testfragments2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class Activity1 extends FragmentActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.i("TEST", "Activity1 onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity1);

        if(savedInstanceState == null) { //Check if first launch
            DynamicFragment3 f = DynamicFragment3.getInstance(5);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.dynamic_fragment_container, f);
            ft.commit();
        }



        findViewById(R.id.button_1_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment1();
            }
        });


        findViewById(R.id.button_2_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment2();
            }
        });


        findViewById(R.id.button_1_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFragment2();
            }
        });

        findViewById(R.id.button_stack_1_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addStackFragment1();
            }
        });

        findViewById(R.id.button_stack_2_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addStackFragment2();
            }
        });


        findViewById(R.id.button_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeDialogFragment();
            }
        });
    }

    private void makeDialogFragment() {
        MyDialogFragment f = MyDialogFragment.getInstance(0);
        f.show( getSupportFragmentManager(), null);
    }


    private void addStackFragment1() {
        StackFragment1 f = StackFragment1.getInstance(0);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.dynamic_fragment_container, f);
        ft.addToBackStack(null);
        ft.commit();
    }

    private void addStackFragment2() {
        StackFragment2 f = StackFragment2.getInstance(0);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.dynamic_fragment_container, f);
        ft.addToBackStack(null);
        ft.commit();
    }


    private void openFragment1() {
        DynamicFragment1 f = new DynamicFragment1();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.dynamic_fragment_container, f, "QWE");
        ft.commit();
    }



    private void openFragment2() {
        DynamicFragment2 f = new DynamicFragment2();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.dynamic_fragment_container, f);
        ft.commit();
    }



    private void removeFragment2() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment f = getSupportFragmentManager().findFragmentByTag("QWE");
        if(f == null){
            Toast.makeText(this, "No fragment", Toast.LENGTH_LONG).show();
            return;
        }
        ft.remove(f);
        ft.commit();
    }


    @Override
    protected void onStart() {
        Log.i("TEST", "Activity1 onStart()");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i("TEST", "Activity1 onResume()");
        super.onResume();
    }


    @Override
    protected void onPause() {
        Log.i("TEST", "Activity1 onPause()");
        super.onPause();
    }


    @Override
    protected void onStop() {
        Log.i("TEST", "Activity1 onStop()");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.i("TEST", "Activity1 onDestroy()");
        super.onDestroy();
    }
}
